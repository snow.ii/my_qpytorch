import numpy as np
import mat73

fig_name = 'qat_fp_fir_3bnn'


N_iter = 10000 # 10000 # 2000 # 25000 # 100 # 184000 

N_1 = 255  # 3; # number of FIR1 coefficients (always odd value)
N_2 = 127  # 3; # number of FIR2 coefficients (always odd value)
N_3 = 123  # 3; # number of FIR2 coefficients (always odd value)

fs = 368.64 # sample rate

mat     = mat73.loadmat("./data/data_in_0.mat")
x_t       = mat["x"]
d_t       = mat["d"]
flt_err = mat["flt_err"]

kx = x_t.std() * 3
kd = d_t.std() * 3
x_t = x_t / kx
d_t = d_t / kd

# lr = 2**(-37) # 0.001

N_win = 1024 # window size
N_fft = 2048 # fft size for PSD

# Coeffcicients (initial state)
c_1_sparse = c_2_sparse = c_3_sparse = 2  # 0

c_1_lags = (np.arange(-np.floor(N_1/2),np.floor(N_1/2)+1, 1 ) * (c_1_sparse+1)).astype(int)
c_1 = np.zeros([1, N_1], dtype="complex128")
c_1[0, int(np.floor(N_1/2))] = 0.1

c_2_lags = (np.arange(-np.floor(N_2/2),np.floor(N_2/2)+1, 1 ) * (c_2_sparse+1)).astype(int)
c_2 = np.zeros([1, N_2], dtype="complex128")
c_2[0, int(np.floor(N_2/2))] = 0.1

c_3_lags = (np.arange(-np.floor(N_3/2),np.floor(N_3/2)+1, 1 ) * (c_3_sparse+1)).astype(int)
c_3 = np.zeros([1, N_3], dtype="complex128")
c_3[0, int(np.floor(N_3/2))] = 0.1

# LMS block size
block_size   = 2048
point_start  = 601
point_end    = 2048-600

sig_bound    = [-37.5, 37.5]

params = {
    "checkpoints": "./data",
    'model_kwargs':{

    },
    "wl_weight": 8, # 6,
    "weight_rounding": "nearest",
    "wl_grad": 16, # 8,
    "grad_rounding": "stochastic",
    "wl_activate": 16, # 8,
    "activate_rounding": "nearest",
    "wl_error": 16, # 8,
    "error_rounding": "stochastic",
    "wl_rand": 16,
    "seed": 100,
    "batch_size": 128,
    "num_workers": 4
}

class Arguments:
    def __init__(self, params):
        num_types = ["weight", "activate", "grad", "error", "momentum"]
        for num in num_types:
            setattr(self, f"wl_{num}", -1)
            setattr(self, f"fl_{num}", -1)
            setattr(self, f"{num}-rounding", 'stochastic')
            for key, value in params.items():
                setattr(self, key, value)

args = Arguments(params)
