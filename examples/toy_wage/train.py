import argparse
import os
import sys
import torch
import torch.nn.functional as F
import torchvision.transforms as transforms
import torchvision.datasets as datasets
import utils
import wage_qtorch
import numpy as np
from tensorboardX import SummaryWriter
from torch.utils.data.sampler import SubsetRandomSampler
from pathlib import Path
from hypers import *
from fir_utils import *
from wage_initializer import wage_init_
from models import FIRModel
import time

seed_all(42)

# TODO картинку размер
# TODO лучший результат по eval на всем брать
# TODO было стало на 6 и на 8 бит. То что тут с нуля результат тоот же

# TODO? шедулер по аналогии с платошедулером.
# TODO? разобраться в теории, где бернулли в градиентах?
# TODO преза по кват вставить в предложение



flt_err = torch.tensor(flt_err).reshape(1,1,-1).type(torch.complex64)
x_t = torch.tensor(x_t)
d_t = torch.tensor(d_t)
c_list = [c_1, c_2, c_3]
lags_list = [c_1_lags, c_2_lags, c_3_lags]
# lr = 2**(-37)

# ********************************************

weight_quantizer = lambda x, scale: wage_qtorch.QW(
    x, args.wl_weight, scale, mode=args.weight_rounding
)
grad_clip = lambda x: wage_qtorch.C(x, args.wl_weight)
if args.wl_weight == -1:
    weight_quantizer = None
if args.wl_grad == -1:
    grad_quantizer = None

from functools import partial

m = partial(
    wage_qtorch.WAGEQuantizer, A_mode=args.activate_rounding, E_mode=args.error_rounding
)

args.model_kwargs.update(
    {
        "quantizer": m,
        "wl_activate": args.wl_activate,
        "wl_error": args.wl_error,
        "wl_weight": args.wl_weight,
    }
)

def schedule(loss):
    if loss > 4:
        return 16.
    elif loss > 2.5:
        return 1/4
    elif loss > 1.:
        return 1/16
    else:
        return 1/16


def eval_loss(sd):
    model = FIRModel(c_list, lags_list, **args.model_kwargs)
    model.load_state_dict(sd)    
    model.eval()
    for name, param in model.named_parameters():
        param.data = weight_quantizer(model.weight_acc[name], model.weight_scale[name])
    core_out = model(x_t.reshape(1, -1).to(torch.complex64))
    e = (d_t.reshape(1, -1) - core_out)
    e = e.detach().numpy()
    core_out = core_out.detach().numpy()
    loss = (e * np.conj(e)).real.mean()
    return loss

# Prepare logging
model = FIRModel(c_list, lags_list, **args.model_kwargs)
print('*************Model architecture**************')
print(model)
print('*********************************************')
L = len(x_t)
ind_start_blocks = np.random.randint(0, L-block_size+1, N_iter)
model.train()

Path(args.checkpoints).mkdir(parents=True, exist_ok=True)
best_loss = 1e6
t0 = time.time()
for iter_cnt in range(N_iter):
    model.zero_grad()
    ind_start = ind_start_blocks[iter_cnt]
    ind_end = ind_start + block_size

    x = x_t[ind_start: ind_end].unsqueeze(0).to(torch.complex64)
    d = d_t[ind_start: ind_end].unsqueeze(0).to(torch.complex64)

    for name, param in model.named_parameters():
        param.data = weight_quantizer(
            model.weight_acc[name], model.weight_scale[name]
        )
    y = model(x)

    e = d - y
    e_flt = convolve(e, flt_err)
    # e_loss = e_flt[:, point_start-1:point_end]
    e_loss = e_flt
    loss = (e_loss * e_loss.conj()).sum()
    loss.backward()
    loss_val = loss.item().real

    if not iter_cnt % 10:
        ev_loss = eval_loss(model.state_dict())
        if ev_loss < best_loss:
            best_loss = ev_loss
            torch.save(model.state_dict(), args.checkpoints+'/weights_best.sd')

    lr = schedule(loss_val)
    grad_quantizer = lambda x: wage_qtorch.QG(
        x, args.wl_grad, args.wl_rand, lr, mode=args.grad_rounding
    )

    for name, param in list(model.named_parameters())[::-1]:
        param.grad.data = grad_quantizer(param.grad.data).data
        w_acc = grad_clip(model.weight_acc[name])
        w_acc -= param.grad.data
        model.weight_acc[name] = w_acc
    
    if not iter_cnt % 100:
        print(f'Loss({iter_cnt}) loss_val={round(loss_val, 5)} log10(loss_val)={round(np.log10(loss_val), 4)} time spent={round(time.time() - t0, 2)}')

sd = torch.load(args.checkpoints+'/weights_best.sd')
loss = eval_loss(sd)
print('HW loss: ', loss)
torch.save(model.state_dict(), args.checkpoints+'/weights_last.sd')

