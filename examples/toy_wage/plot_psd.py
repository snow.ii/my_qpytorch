import torch
from torch import nn
import numpy as np
import torch.nn.functional as F
import random
import os
import matplotlib.pyplot as plt
from hypers import *
from fir_utils import psdmw_c_th6
from models import FIRModel
import wage_qtorch
from functools import partial

# def plot_psd(e, core_out):
#     SS, ff = psdmw_c_th6([x_t*kx, d_t*kd, e*kd, core_out*kd], np.kaiser(N_win, 10), 1, N_fft, fs, 0, 0)
#     font_size = 7

#     fig = plt.figure(dpi=110, figsize=(14, 8))  #15, 12
#     spec=fig.add_gridspec(8, 6)
#     fig.add_subplot(spec[0:8, 0:3])

#     plt.plot(ff, SS[0, :], label="Tx_in")
#     plt.plot(ff, SS[1, :], label="Tx_out")
#     plt.plot(ff, SS[2, :], label="Err")
#     plt.plot(ff, SS[3, :], label="Core_out")
#     plt.grid()
#     plt.xticks(fontsize=font_size)
#     plt.yticks(fontsize=font_size)
#     plt.xlabel("freq, MHz", fontsize=font_size)
#     plt.ylabel("Amplitude, dB", fontsize=font_size)
#     plt.ylim(-20, 90)
#     plt.xlim(-fs/2, fs/2)

#     mk_tlt = "N_iter = "+str(N_iter)+";\n N_1_coe = "+\
#             str(N_1)+"; N_2_coe = "+str(N_2)+"; N_3_coe = "+str(N_3)
#     plt.title(mk_tlt, fontsize=8)
#     plt.legend(["x", "desirable", "err", "core_out", "|H_fir1|", "|H_fir2|", "|H_fir3|"])

#     plt.subplots_adjust(wspace=0.6,hspace=1.9)
#     plt.savefig(f'./plots/performance.png', bbox_inches='tight', pad_inches=0.2)


def plot_psd(e, core_out, title, out_path=f'./plots/performance.png'):
    font_size = 7
    SS, ff = psdmw_c_th6([x_t*kx, d_t*kd, e*kd, core_out*kd], np.kaiser(N_win, 10), 1, N_fft, fs, 0, 0)
    plt.plot(ff, SS[0, :], label="Tx_in")
    plt.plot(ff, SS[1, :], label="Tx_out")
    plt.plot(ff, SS[2, :], label="Err")
    plt.plot(ff, SS[3, :], label="Core_out")
    plt.grid()
    plt.xticks(fontsize=font_size)
    plt.yticks(fontsize=font_size)
    plt.xlabel("freq, MHz", fontsize=font_size)
    plt.ylabel("Amplitude, dB", fontsize=font_size)
    plt.ylim(-20, 90)
    plt.xlim(-fs / 2, fs / 2)
    plt.title(title, fontsize=10)
    plt.legend(["x", "desirable", "err", "core_out"])
    plt.savefig(out_path, bbox_inches='tight', pad_inches=0.2)
    


if __name__ == '__main__':
   
    m = partial(wage_qtorch.WAGEQuantizer, A_mode=args.activate_rounding, E_mode=args.error_rounding)   
    
    kwargs = {
        "quantizer": m,
        "wl_activate": args.wl_activate,
        "wl_error": args.wl_error,
        "wl_weight": args.wl_weight,
    }
    x_t = torch.tensor(x_t)
    d_t = torch.tensor(d_t)
    c_list = [c_1, c_2, c_3]
    lags_list = [c_1_lags, c_2_lags, c_3_lags]
    
    model = FIRModel(c_list, lags_list, **kwargs)
    model.load_state_dict(torch.load(args.checkpoints+'/weights_best.sd'))
    model.eval()
    core_out = model(x_t.reshape(1, -1).to(torch.complex64))
    e = (d_t.reshape(1, -1) - core_out)
    e = e.detach().numpy()
    core_out = core_out.detach().numpy()
    loss = (e * np.conj(e)).real.mean()
    print('HW loss: ', loss)

    x_t = x_t.numpy()
    d_t = d_t.numpy()

    title = 'QAT (8 bits) with starting weights like \n [0, 0, .. 0.1, .. 0, 0 ] for every layer'
    plot_psd(e, core_out, title, out_path=f'./plots/performance.png')
    
