import torch
from torch import nn
import numpy as np
import torch.nn.functional as F
import random
import os
import matplotlib.pyplot as plt
from hypers import *

def psdmw_c_th6(s, weight_win, noverlap, nfft, Fs, Fc, plot_on, l="", line_w=1):
    """
    s, f = psdmw_c_th6(s, weight_win, noverlap, nfft, Fs, Fc, fig_number, line_w)
    psdmw( s, weight_win, overlap, Fs, Fc, fig_number )
    Plots PSD estimates of s-matrix rows by frame-based accumulation
    with windowing
    Created 2015-05-14 by Broneslav KIselman (ID: 00251134)
    Changed 2018-12-03 by Zelentsova Yana    (ID: 00489520)
    Transformed 2020-10-19 by Limuzi (ID: 00513974) from matlab to python
    :param s: matrix with signals in rows
    :param weight_win: weighting window (row vector)
    :param noverlap: settings for short-time representation (see BUFFER)
    :param nfft: FFT size
    :param Fs: samplerate, MHz
    :param Fc: central frequensy, MHz
    :param plot_on:
    :param fig_number: number of figure (if zero, creates new figure)
    :param line_w:
    :param clr_set:
    :return: s - matrix with PSDs in rows
    Examples: s = psdmw( [ x; y; x - y ], hanning( 2^10 )', 0, 2^12, 368.64, 1800, 7 )
    """
    # Number of signals to analyze

    l_s = len(s)
    Nsignals = 0
    s_repck = []
    for i in range(l_s):
        if len(s[i].shape) > 1:   # if tuple size = 1
            N_s = s[i].shape[0]
            Nsignals = Nsignals + N_s
            for j in range(N_s):
                s_repck.append(s[i][j])
        else:
            Nsignals = Nsignals + 1
            s_repck.append(s[i]) 

    if l == '':
        l = list()
        [l.append('x' + str(i)) for i in range(Nsignals)]
    # Preparing the matrix for PSDs
    # SS = np.zeros([Nsignals, nfft])
    # Length of frame
    framesize = len(weight_win)
    # Energy of weight window
    win_energy = sum(weight_win**2)
    # Number of frames to process

    SS  = np.zeros([Nsignals, nfft])
    for nn in range(Nsignals):
        # Decomposing a signal into a quantity of frames
        L = s_repck[nn].shape[0]
        Nframes = int(np.fix((L - noverlap) / (framesize - noverlap)))
        s_buff_str = np.arange(0, L, framesize-noverlap)
        S1 = np.zeros([1, nfft])
        for kk in range(Nframes):
            s1 = s_repck[nn][s_buff_str[kk]:s_buff_str[kk]+framesize]
            S1 = S1 + abs(np.fft.fft(weight_win*s1, nfft))**2
        # Normalizing
        SS[nn, :] = 10 * np.log10(np.fft.fftshift(S1 * (1 / (Nframes * win_energy))))

    # SS   = np.zeros([Nsignals, nfft])
    # for i in range(Nsignals):
    #     L = ss_repck[nn].shape[0]
    #     Nframes = int(np.fix((L - noverlap) / (framesize - noverlap)))
    #     ff, SS = signal.welch(ss_repck[i], Fs, weight_win, nfft = nfft,  return_onesided = False) 
    #     SS[i, :] = 10.0*np.log10(np.fft.fftshift(SS)* ((win_energy)))

    # Vector of frequensies
    f = Fc + (np.arange(nfft)/nfft - 0.5) * Fs
    
    if plot_on:
        fig, ax = plt.subplots()
        for nn in range(Nsignals):
            ax.plot(f, SS[nn], label=l[nn], lw=line_w)
        plt.xlabel('Frequency/MHz')
        plt.ylabel("PSD/dB")
        plt.grid()
        plt.legend(loc='upper right')
        plt.show()
        
        # get_current_fig_manager().window.raise_()
        # fig=gcf()
        # fig.canvas.manager.window.raise_()
        
    return SS, f


def convolve(x, fltr):
    fltr = torch.flip(fltr, dims=[-1, ])
    res = F.conv1d(x, fltr, padding='same')
    return res


def make_shifts(a, shifts):
    cats = []
    for s in shifts:
        if s > 0:
            row = F.pad(a, (0, s))[:, s:]  # [:, int(s):]
        elif s < 0:
            row = F.pad(a, (abs(s), 0))[:, :s]
        else:
            row = a
        cats.append(row)
    A = torch.cat(cats, dim=0)
    return A

def seed_all(seed=1029):
    random.seed(seed)
    os.environ["PYTHONHASHSEED"] = str(seed)
    np.random.seed(seed)
    torch.manual_seed(seed)


def fmt(v, roun=1):
    pw = np.floor(np.log10(v))
    num = np.round(v / 10. ** pw, roun)
    return ' '.join([f'{n}e{int(p)}' for n, p in zip(num, pw)])

