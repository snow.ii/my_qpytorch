import argparse
import os
import sys
import torch
import torch.nn.functional as F
import torchvision.transforms as transforms
import torchvision.datasets as datasets
import utils
import wage_qtorch
import numpy as np
from tensorboardX import SummaryWriter
from torch.utils.data.sampler import SubsetRandomSampler
from pathlib import Path
from hypers import *
from fir_utils import *
from wage_initializer import wage_init_


class ShiftMM(nn.Module):
    def __init__(self, init_weight, shift, f_quant):
        super().__init__()
        self.quant = f_quant()
        self.weight = nn.Parameter(torch.tensor(init_weight, dtype=torch.float32))
        self.shift = shift

    def forward(self, x):
        x = make_shifts(x, -self.shift)
        y = torch.mm(self.weight, x)
        return self.quant(y)


class FIRModel(nn.Module):
    def __init__(self, c_list, lags_list, quantizer, 
                 wl_activate=16, wl_error=16, wl_weight=16):
        super().__init__()
        f_quant = lambda: quantizer(wl_activate, wl_error)
        f_clip = lambda x, b: np.sign(x) * np.clip(np.abs(x), 1/2**b, 1 - 1/2**b) * (x != 0) 
        self.quant0_r = quantizer(wl_activate, -1)
        self.quant0_i = quantizer(wl_activate, -1)
        self.quant1_r = quantizer(-1, wl_error)
        self.quant1_i = quantizer(-1, wl_error)
        
        self.c_r = nn.ModuleList()
        self.c_i = nn.ModuleList()
        for c, lags in zip(c_list, lags_list):
            self.c_r.append(ShiftMM(f_clip(c.real, wl_weight), lags, f_quant))
            self.c_i.append(ShiftMM(f_clip(c.imag, wl_weight), lags, f_quant))

        self.weight_scale = {}
        self.weight_acc = {}
        for name, param in self.named_parameters():
            assert "weight" in name
            wage_init_(param, wl_weight, name, self.weight_scale, factor=1.0)
            self.weight_acc[name] = param.data


    def forward(self, x):
        x_r, x_i = x.real, x.imag
        x_r = self.quant0_r(x_r)
        x_i = self.quant0_i(x_i)
        n = len(self.c_r)
        for idx in range(n):
            y_r = self.c_r[idx](x_r) - self.c_i[idx](x_i)
            y_i = self.c_r[idx](x_i) + self.c_i[idx](x_r)
            x_r, x_i = y_r, y_i
        x_r = self.quant1_r(x_r)
        x_i = self.quant1_i(x_i)
        return torch.complex(x_r, x_i)
