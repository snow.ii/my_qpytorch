from torch import nn

class Base(nn.Module):
    args = list()
    kwargs = dict()
    def __init__(self):
        super(Base, self).__init__()
