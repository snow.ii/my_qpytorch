from functools import partial
from typing import Any, cast, Dict, List, Optional, Union
from .wage_initializer import wage_init_
import torch
import torch.nn as nn
from models.base import Base


AVG_POOL = True

__all__ = [
    "VGG8",
    "VGG11",
    "VGG13",
    "VGG16",
    "VGG19",
]


class VGG(Base):
    def __init__(
        self, cfg: dict, batch_norm: bool, dropout: Any = None, num_linears: int = 3, 
        num_classes: int = 10, quantizer: Any = None, 
        wl_weight: int=2, wl_activate: int = 8, wl_error: int = 8, bias: bool = False,
        avgpool=False, pretrained=False, **kwargs
        ) -> None:
        super().__init__()
        if pretrained:
            raise NotImplementedError("Loading pretrained weights not supported.")
        quant = lambda: quantizer(wl_activate, wl_error)
        self.avgpool = avgpool
        in_channels = 3
        self.features = [quantizer(wl_activate, -1),]
        for v in cfg:
            if v == "M":
                self.features += [nn.MaxPool2d(kernel_size=2, stride=2)]
            else:
                v = cast(int, v)
                conv2d = nn.Conv2d(in_channels, v, kernel_size=3, padding=1, bias=bias)
                if batch_norm:
                    self.features += [conv2d, nn.BatchNorm2d(v), nn.ReLU(inplace=True)]
                else:
                    self.features += [conv2d, nn.ReLU(inplace=True)]
                self.features += [quant()]
                in_channels = v

        self.features = nn.Sequential(*self.features)

        """
        # TODO где бернулли при расчете градиента?
        # TODO examples/tutorial, lp_train, SWALP, IBM8 , для ResNet аналогично  
        """
        if avgpool:
            self.avgpool_layer = nn.AdaptiveAvgPool2d((7, 7))
            n_channels = [512 * 7 * 7] + [4096] * (num_linears - 1) + [num_classes]
        else:
            n_channels = [512 * 4 * 4] + [4096] * (num_linears - 1) + [num_classes]
        self.classifier = nn.Sequential()
        for i in range(len(n_channels) - 1):
            self.classifier.append(nn.Linear(n_channels[i], n_channels[i + 1], bias=bias))
            if i < len(n_channels) - 2:
                self.classifier.append(nn.ReLU(True))
                self.classifier.append(quant())
                if type(dropout) == float:
                    self.classifier.append(nn.Dropout(p=dropout))
        self.classifier.append(quantizer(-1, wl_error))

        self.weight_scale = {}
        self.weight_acc = {}
        for name, param in self.named_parameters():
            assert "weight" in name
            wage_init_(param, wl_weight, name, self.weight_scale, factor=1.0)
            self.weight_acc[name] = param.data


    def forward(self, x: torch.Tensor) -> torch.Tensor:
        x = self.features(x)
        if self.avgpool:
            x = self.avgpool_layer(x)
        x = torch.flatten(x, 1)
        x = self.classifier(x)
        return x


cfgs: Dict[str, List[Union[str, int]]] = {
    "O": [128, 128, 'M', 256, 256, 'M', 512, 512, 'M'],
    "A": [64, "M", 128, "M", 256, 256, "M", 512, 512, "M", 512, 512, "M"],
    "B": [64, 64, "M", 128, 128, "M", 256, 256, "M", 512, 512, "M", 512, 512, "M"],
    "D": [64, 64, "M", 128, 128, "M", 256, 256, 256, "M", 512, 512, 512, "M", 512, 512, 512, "M"],
    "E": [64, 64, "M", 128, 128, "M", 256, 256, 256, 256, "M", 512, 512, 512, 512, "M", 512, 512, 512, 512, "M"],
}

class VGG8(VGG):
    def __init__(self,
                quantizer,
                wl_activate,
                wl_error,
                num_classes=10,
                depth=16,
                batch_norm=False,
                wl_weight=-1,
                writer=None,
                **kwargs
                ):
        
        super().__init__(
            cfg=cfgs['O'],
            batch_norm=batch_norm,
            dropout=None,
            num_linears=2,
            num_classes=num_classes,
            quantizer=quantizer,
            wl_activate=wl_activate,
            wl_error=wl_error,
            wl_weight=wl_weight,
            avgpool=AVG_POOL,
            **kwargs
        )
        pass

class VGG11(VGG):
    def __init__(self,
                quantizer,
                wl_activate,
                wl_error,
                num_classes=10,
                depth=16,
                batch_norm=False,
                wl_weight=-1,
                writer=None,
                **kwargs
                ):
        
        super().__init__(
            cfg=cfgs['A'],
            batch_norm=batch_norm,
            dropout=None,
            num_linears=3,
            num_classes=num_classes,
            quantizer=quantizer,
            wl_activate=wl_activate,
            wl_error=wl_error,
            wl_weight=wl_weight,
            avgpool=AVG_POOL,
            **kwargs
        )
        pass


class VGG13(VGG):
    def __init__(self,
                quantizer,
                wl_activate,
                wl_error,
                num_classes=10,
                depth=16,
                batch_norm=False,
                wl_weight=-1,
                writer=None,
                **kwargs
                ):
        
        super().__init__(
            cfg=cfgs['B'],
            batch_norm=batch_norm,
            dropout=None,
            num_linears=3,
            num_classes=num_classes,
            quantizer=quantizer,
            wl_activate=wl_activate,
            wl_error=wl_error,
            wl_weight=wl_weight,
            avgpool=AVG_POOL,
            **kwargs
        )
        pass


class VGG16(VGG):
    def __init__(self,
                quantizer,
                wl_activate,
                wl_error,
                num_classes=10,
                depth=16,
                batch_norm=False,
                wl_weight=-1,
                writer=None,
                **kwargs
                ):
        
        super().__init__(
            cfg=cfgs['D'],
            batch_norm=batch_norm,
            dropout=None,
            num_linears=3,
            num_classes=num_classes,
            quantizer=quantizer,
            wl_activate=wl_activate,
            wl_error=wl_error,
            wl_weight=wl_weight,
            avgpool=AVG_POOL,
            **kwargs
        )
        pass


class VGG19(VGG):
    def __init__(self,
                quantizer,
                wl_activate,
                wl_error,
                num_classes=10,
                depth=16,
                batch_norm=False,
                wl_weight=-1,
                writer=None,
                **kwargs
                ):
        
        super().__init__(
            cfg=cfgs['E'],
            batch_norm=batch_norm,
            dropout=None,
            num_linears=3,
            num_classes=num_classes,
            quantizer=quantizer,
            wl_activate=wl_activate,
            wl_error=wl_error,
            wl_weight=wl_weight,
            avgpool=AVG_POOL,
            **kwargs
        )
        pass
