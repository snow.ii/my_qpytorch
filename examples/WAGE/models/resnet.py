from functools import partial
from typing import Any, Callable, List, Optional, Type, Union
from .wage_initializer import wage_init_
import torch
import torch.nn as nn
from torch import Tensor
from models.base import Base
import torchvision.models as torch_models

__all__ = [
    "ResNet18",
    "ResNet50",
    "ResNet101",
]


def conv3x3(in_planes: int, out_planes: int, stride: int = 1, groups: int = 1, dilation: int = 1) -> nn.Conv2d:
    """3x3 convolution with padding"""
    return nn.Conv2d(
        in_planes,
        out_planes,
        kernel_size=3,
        stride=stride,
        padding=dilation,
        groups=groups,
        bias=False,
        dilation=dilation,
    )


def conv1x1(in_planes: int, out_planes: int, stride: int = 1) -> nn.Conv2d:
    """1x1 convolution"""
    return nn.Conv2d(in_planes, out_planes, kernel_size=1, stride=stride, bias=False)


class BasicBlock(nn.Module):
    expansion: int = 1

    def __init__(
        self,
        inplanes: int,
        planes: int,
        stride: int = 1,
        downsample: Optional[nn.Module] = None,
        groups: int = 1,
        base_width: int = 64,
        dilation: int = 1,
        quant: Any = None
    ) -> None:
        super().__init__()
        if groups != 1 or base_width != 64:
            raise ValueError("BasicBlock only supports groups=1 and base_width=64")
        if dilation > 1:
            raise NotImplementedError("Dilation > 1 not supported in BasicBlock")
        # Both self.conv1 and self.downsample layers downsample the input when stride != 1
        self.conv1 = conv3x3(inplanes, planes, stride)
        self.q1 = quant()
        self.relu = nn.ReLU(inplace=True)
        self.conv2 = conv3x3(planes, planes)
        self.q2 = quant()
        self.downsample = downsample
        self.stride = stride

    def forward(self, x: Tensor) -> Tensor:
        identity = x
        out = self.conv1(x)
        out = self.relu(out)
        out = self.q1(out)
        out = self.conv2(out)
        
        if self.downsample is not None:
            identity = self.downsample(x)
        out += identity

        out = self.relu(out)
        out = self.q2(out)

        return out


class Bottleneck(nn.Module):
    expansion: int = 4

    def __init__(
        self,
        inplanes: int,
        planes: int,
        stride: int = 1,
        downsample: Optional[nn.Module] = None,
        groups: int = 1,
        base_width: int = 64,
        dilation: int = 1,
        quant: Any = None,
    ) -> None:
        super().__init__()
        width = int(planes * (base_width / 64.0)) * groups
        # Both self.conv2 and self.downsample layers downsample the input when stride != 1
        self.conv1 = conv1x1(inplanes, width)
        self.q1 = quant()
        self.conv2 = conv3x3(width, width, stride, groups, dilation)
        self.q2 = quant()
        self.conv3 = conv1x1(width, planes * self.expansion)
        self.q3 = quant()
        self.relu = nn.ReLU(inplace=True)
        self.downsample = downsample
        self.stride = stride

    def forward(self, x: Tensor) -> Tensor:
        # hbzhbz add here quants
        identity = x

        out = self.conv1(x)
        out = self.q1(out)
        out = self.relu(out)

        out = self.conv2(out)
        out = self.q2(out)
        out = self.relu(out)

        out = self.conv3(out)

        if self.downsample is not None:
            identity = self.downsample(x)

        out += identity
        out = self.relu(out)
        out = self.q3(out)
        return out


class ResNet(Base):
    def __init__(
        self,
        block: Type[Union[BasicBlock, Bottleneck]],
        layers: List[int],
        num_classes: int = 1000,
        groups: int = 1,
        width_per_group: int = 64,
        quantizer: Any = None, 
        wl_weight: int=2, 
        wl_activate: int = 8, 
        wl_error: int = 8,
        pretrained: str = '',
        **kwargs
        ) -> None:
        super().__init__()

        quant = lambda: quantizer(wl_activate, wl_error)
        self.quant_first = quantizer(wl_activate, -1)

        self.inplanes = 64
        self.dilation = 1
        replace_stride_with_dilation = [False, False, False]
        self.groups = groups
        self.base_width = width_per_group
        self.conv1 = nn.Conv2d(3, self.inplanes, kernel_size=7, stride=2, padding=3, bias=False)
        self.relu = nn.ReLU(inplace=True)
        self.maxpool = nn.MaxPool2d(kernel_size=3, stride=2, padding=1)
        self.q = quant()
        self.layer1 = self._make_layer(block, 64, layers[0], quant=quant)
        self.layer2 = self._make_layer(block, 128, layers[1], stride=2, dilate=replace_stride_with_dilation[0], quant=quant)
        self.layer3 = self._make_layer(block, 256, layers[2], stride=2, dilate=replace_stride_with_dilation[1], quant=quant)
        self.layer4 = self._make_layer(block, 512, layers[3], stride=2, dilate=replace_stride_with_dilation[2], quant=quant)
        self.avgpool = nn.AdaptiveAvgPool2d((1, 1))
        self.fc = nn.Linear(512 * block.expansion, num_classes)
        self.quant_last = quantizer(-1, wl_error)
        
        self.weight_scale = {}
        self.weight_acc = {}
        pretr_sd = getattr(torch_models, pretrained)(pretrained=True).state_dict() if pretrained != '' else None
        prev_fan_in, prev_name = None, ''
        for name, param in self.named_parameters():
            mode_value = prev_fan_in if name == prev_name + '.bias' else None
            assert "weight" in name or mode_value is not None
            pw = pretr_sd[name] if pretr_sd is not None else None
            prev_fan_in = wage_init_(param, wl_weight, name, self.weight_scale, 
                                        factor=1.0, mode_value=mode_value, pretr_weight=pw)
            prev_name = name.split('.')[0]
            self.weight_acc[name] = param.data
                    

    def _make_layer(
        self,
        block: Type[Union[BasicBlock, Bottleneck]],
        planes: int,
        blocks: int,
        stride: int = 1,
        dilate: bool = False,
        quant: Any = None
    ) -> nn.Sequential:
        downsample = None
        previous_dilation = self.dilation
        if dilate:
            self.dilation *= stride
            stride = 1
        if stride != 1 or self.inplanes != planes * block.expansion:
            downsample = nn.Sequential(
                conv1x1(self.inplanes, planes * block.expansion, stride),
                quant()
            )

        layers = []
        layers.append(
            block(
                self.inplanes, planes, stride, downsample, self.groups, self.base_width, previous_dilation, quant
            )
        )
        self.inplanes = planes * block.expansion
        for _ in range(1, blocks):
            layers.append(
                block(
                    self.inplanes,
                    planes,
                    groups=self.groups,
                    base_width=self.base_width,
                    dilation=self.dilation,
                    quant=quant,
                )
            )

        return nn.Sequential(*layers)

    def _forward_impl(self, x: Tensor) -> Tensor:
        # See note [TorchScript super()]
        x = self.quant_first(x)
        x = self.conv1(x)
        x = self.relu(x)
        x = self.maxpool(x)
        x = self.q(x)

        x = self.layer1(x)
        x = self.layer2(x)
        x = self.layer3(x)
        x = self.layer4(x)

        x = self.avgpool(x)
        x = torch.flatten(x, 1)
        x = self.fc(x)
        x = self.quant_last(x)
        return x

    def forward(self, x: Tensor) -> Tensor:
        return self._forward_impl(x)


class ResNet18(ResNet):
    def __init__(self,
                quantizer,
                wl_activate,
                wl_error,
                num_classes=10,
                wl_weight=-1,
                pretrained=False,
                **kwargs):
        super().__init__(block=BasicBlock, 
                         layers=[3, 4, 6, 3], 
                         num_classes=num_classes,
                         quantizer=quantizer,
                         wl_activate=wl_activate,
                         wl_error=wl_error,
                         wl_weight=wl_weight,
                         pretrained='resnet18' if pretrained else '',
                         **kwargs)
        pass


class ResNet50(ResNet):
    def __init__(self,
                quantizer,
                wl_activate,
                wl_error,
                num_classes=10,
                wl_weight=-1,
                pretrained=False,
                **kwargs):
        super().__init__(block=Bottleneck, 
                         layers=[3, 4, 6, 3], 
                         num_classes=num_classes,
                         quantizer=quantizer,
                         wl_activate=wl_activate,
                         wl_error=wl_error,
                         wl_weight=wl_weight,
                         pretrained='resnet50' if pretrained else '',
                         **kwargs)
        pass


class ResNet101(ResNet):
    def __init__(self,
                quantizer,
                wl_activate,
                wl_error,
                num_classes=10,
                wl_weight=-1,
                pretrained=False,
                **kwargs):
        super().__init__(block=Bottleneck, 
                         layers=[3, 4, 23, 3], 
                         num_classes=num_classes,
                         quantizer=quantizer,
                         wl_activate=wl_activate,
                         wl_error=wl_error,
                         wl_weight=wl_weight,
                         pretrained='resnet101' if pretrained else '',
                         **kwargs)
        pass

