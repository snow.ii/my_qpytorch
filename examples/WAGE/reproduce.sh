#! /bin/bash

python train.py \
       --dataset CIFAR10 \
       --data_path ./data \
       --dir ./checkpoints/VGG8_2_8_8_8 \
       --model VGG8 \
       --epochs=300 \
       --wl-weight 4 \
       --weight-rounding nearest \
       --wl-grad 8 \
       --grad-rounding stochastic \
       --wl-activate 8 \
       --activate-rounding nearest \
       --wl-error 8 \
       --error-rounding stochastic \
       --wl-rand 16 \
       --seed 100 \
       --batch_size 128 \
       --qtorch;
