import torch
import torchvision.models as models
import torchvision.transforms as transforms
from PIL import Image
import torch.nn as nn
from pathlib import Path

class TmpNet(nn.Module):
	def __init__(self):
		super().__init__()
		self.linear1 = nn.Linear(10, 4)
		self.linear2 = nn.Linear(4, 1)
		for name, m in self.named_modules():
			print(name, m)

tmp_model = TmpNet()

# # print(models.vgg11_bn(pretrained=False))
q_sd = torch.load('./checkpoints/ResNet50_wage2888/best_model.sd')
# sd = models.resnet50(pretrained=True).state_dict()
sd = getattr(models, 'resnet50')(pretrained=True).state_dict()
print(sd)
exit()
print('ResNet****************************')
# m = models.resnet50(pretrained=True)
# path = './weights/ResNet/resnet50.sd'

m = models.resnet101(pretrained=True)
path = './weights/ResNet/resnet101.sd'

folder = Path(path).parent.resolve()
folder.mkdir(parents=True, exist_ok=True)
torch.save(m.state_dict(), path)
exit()
print('VGG11****************************')
print(models.vgg11(pretrained=False))
print('VGG13****************************')
print(models.vgg13(pretrained=False))
print('VGG16****************************')
print(models.vgg16(pretrained=False))
print('VGG19****************************')
print(models.vgg19(pretrained=False))
print('ResNet18****************************')
print(models.resnet18(pretrained=False))

from torchvision.models import VGG, vgg11

model = VGG(num_classes=10)