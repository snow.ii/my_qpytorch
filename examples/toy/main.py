import numpy as np
import matplotlib.pyplot as plt
from params import *
from utils import *
from tqdm import tqdm
import torch.nn as nn
from torch.optim import SGD

seed_all(42)

class ShiftMM(nn.Module):
    def __init__(self, init_weight, shift):
        super().__init__()
        self.weight = nn.Parameter(torch.tensor(init_weight))
        self.shift = shift

    def forward(self, x):
        x = make_shifts(x, -self.shift)
        return torch.mm(self.weight, x)


class FIRModel(nn.Module):
    def __init__(self, c_list, lags_list):
        super().__init__()
        self.c_r = nn.ModuleList()
        self.c_i = nn.ModuleList()
        for c, lags in zip(c_list, lags_list):
            self.c_r.append(ShiftMM(c.real, lags))
            self.c_i.append(ShiftMM(c.imag, lags))

    def forward(self, x):
        x_r, x_i = x.real, x.imag
        n = len(self.c_r)
        for idx in range(n):
            y_r = self.c_r[idx](x_r) - self.c_i[idx](x_i)
            y_i = self.c_r[idx](x_i) + self.c_i[idx](x_r)
            x_r, x_i = y_r, y_i
        return torch.complex(x_r, x_i)

# TODO дебажить toy-wage
# TODO нормировать веса чтобы исходные были [-1, 1]
# TODO инференс qat-fp

flt_err = torch.tensor(flt_err).reshape(1,1,-1).type(torch.complex128)
x_t = torch.tensor(x_t)
d_t = torch.tensor(d_t)

c_list = [c_1, c_2, c_3]
lags_list = [c_1_lags, c_2_lags, c_3_lags]

model = FIRModel(c_list, lags_list)
optimizer = SGD(model.parameters(), lr=lr)
# ***************************
L = len(x_t)  # data length

# Calculating blocks start indices
ind_start_blocks = np.random.randint(0, L-block_size+1, N_iter)
model.train()
for iter_cnt in tqdm(range(N_iter)):
    # Block bounds indices
    optimizer.zero_grad()
    ind_start = ind_start_blocks[iter_cnt]
    ind_end = ind_start + block_size

    x = x_t[ind_start: ind_end].unsqueeze(0)
    d = d_t[ind_start: ind_end].unsqueeze(0)

    y = model(x)
    
    e = d - y
    e_flt = convolve(e, flt_err)

    e_loss = e_flt[:, point_start-1:point_end]
    loss = (e_loss * e_loss.conj()).sum()
    loss.backward()
    loss_val = loss.item().real

    optimizer.step()

    if iter_cnt and iter_cnt % 5000 == 0: # 5000 == 0:
        torch.save(model.state_dict(), f'./data/weights{iter_cnt}.pkl')

    if not iter_cnt % 100:
        print(f'Loss({iter_cnt}) loss_val={int(loss_val)} log10(loss_val)={round(np.log10(loss_val), 4)}')

model.eval()
core_out = model(x_t.reshape(1, -1))
e = (d_t.reshape(1, -1) - core_out)
e = e.detach().numpy()
core_out = core_out.detach().numpy()

print('HW loss: ', (e * np.conj(e)).real.mean())

dct = dict()
torch.save(model.state_dict(), './data/weights.sd')
plot_psd(e, core_out)